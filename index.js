const express = require('express')
const app = express();
app.use(express.json());
const port = 3300;
app.listen(port);
console.log(`Server is running on port ${port}`);
const path = require('path');

app.use('/public', express.static('public'))

app.get("/", (request, response) => {
    response.send("<h1>Página Principal</h1>")
})

let productos = [
    {'id': 1, 'nombre': "Celular", 'precio': 90000, photo: 'https://puertoliquidaciones.com/wp-content/uploads/2021/01/Samsung-Galaxy-Z-Flip-5G-Mystic-Bronze-frontimage_800x800.jpg'},
    {'id': 2, 'nombre': "Notebook", 'precio': 150000, photo: 'https://mallweb.com.ar/media/catalog/product/cache/1/image/900x/9df78eab33525d08d6e5fb8d27136e95/g/i/gimage_26759.jpg'},
    {'id': 3, 'nombre': "Mouse", 'precio': 5000, photo: 'http://snpi.dell.com/snp/images/products/large/dell_wm126_wireless_optical_mouse_blk_3_l.jpg'},
    {'id': 4, 'nombre': "Teclado", 'precio': 3000, photo: 'https://d3ugyf2ht6aenh.cloudfront.net/stores/829/696/products/kb8700_11-a322dd42ab644aec8716002987941021-1024-1024.jpg'},
    {'id': 5, 'nombre': "HDD Case", 'precio': 1000, photo: 'https://m.media-amazon.com/images/I/41zlrXJ9RTL.jpg'},
]
//let productos = []

//LO DEFINIMOS AQUÍ, Y SE CARGA EN EL APP.POST #/CART
let cart = []

//CONECTARNOS A LA BASE DE DATOS
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'ecommerce_clase'
});

connection.connect();

connection.query('SELECT * FROM products', function (error, results, fields) {
  if (error) throw error;

  productos = results;
  console.log('productos: ', productos);
});

connection.end();
/////////////////////////////////////////////////////////////////////////////////

app.get("/productos", (request, response) => {
    response.json(productos)
})

app.post("/cart", (request, response) => {
    //console.log("Estamos en el post")
    const productoPedido = request.body;
    response.json(productoPedido);
    console.log('producto pedido con click, arribado al servidor: ',productoPedido);
    productoPedido.id = Number(productoPedido.id)
    const productoEncontrado = productos.find(prodItem => prodItem.id === productoPedido.id)
    console.log("Producto encontrado en el array 'productos': ", productoEncontrado)
    //productoEncontrado.cant = 1
    //cart.push(productoEncontrado)
    const indice = cart.findIndex(cartItem => cartItem.id === productoEncontrado.id);
    if (indice >= 0){
        cart[indice].cant++;
    }else{
        productoEncontrado.cant = 1;
        cart.push(productoEncontrado);
    }
    console.log('cart[]= ',cart);
})

app.post("/cartdelete", (request, response) => {
    //console.log("Estamos en el post")
    const productoPedido = request.body;
    response.json(productoPedido);
    const indice = cart.findIndex(cartItem => cartItem.id === productoPedido.id);
    let removed = cart.splice(indice, 1);
    console.log('removed= ', removed);
})

app.delete("/cartdelete/:id", (request, response) => {
    //console.log("Estamos en el post")
    const id = Number(request.params.id);
    response.json(id);
    cart = cart.filter(cartItem => cartItem.id !== id);
    console.log('id eliminado del cart[]: ', id);
    console.log('cart[] sin el producto eliminado: ', cart);
})

app.get("/cart", (request, response) => {
    response.json(cart)
})