let misProductos = () => {
    fetch('/productos', { method: 'GET' })
        .then(response => response.json())
        .then(listadoDeProductos => listadoDeProductos.map(
            //    porCadaProducto => console.log(porCadaProducto)
            porCadaProducto => {
                let template =
                    `
            <div class="product slick-slide slick-active" aria-hidden="true" style="width: 263px;">
                <div class="product-img">
                    <img src="${porCadaProducto.photo}" alt="">
                    <div class="product-label">
                        <span class="sale">-30%</span>
                        <span class="new">NEW</span>
                    </div>
                </div>
                <div class="product-body">
                    <p class="product-category">Category</p>
                    <h3 class="product-name"><a href="product.html">${porCadaProducto.nombre}</a></h3>
                    <h4 class="product-price">$${porCadaProducto.precio} <del class="product-old-price">${porCadaProducto.precio}</del></h4>
                    <div class="product-rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="product-btns">
                        <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
                        <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
                        <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
                    </div>
                </div>
                <div class="add-to-cart">
                    <button onclick="addToCart('${porCadaProducto.id}')" class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
                </div>
            </div>
            `
                document.querySelector('#contentProducts').innerHTML += template;
            }
        ))
}

misProductos();

let addToCart = (idProducto) => {
    console.log(idProducto);

    let productoASubir = {};
    productoASubir.id = idProducto;
    console.log(productoASubir);

    fetch('/cart', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: JSON.stringify(productoASubir)
    })
        .then(showCart())
}

let showCart = () => {
    //INICIALIZO LOS TOTALES EN LA INTERFAZ
    document.querySelector('#cartList').innerHTML = ""
    let subtotal = 0, cantItems = 0;
    document.querySelector("#cartTotals").innerHTML =
        `
    <small>${cantItems} Item(s) selected</small>
    <h5>SUBTOTAL: $${subtotal}</h5>
    `
    document.querySelector('#cartQty').innerHTML = cantItems;
    ////////////////////////////////////////
    fetch('/cart', { method: 'GET' })
        .then(response => response.json())
        .then(cartList => cartList.map(
            porCadaProducto => {
                let template =
                    `
            <div class="product-widget">
                <div class="product-img">
                    <img src="${porCadaProducto.photo}" alt="">
                </div>
                <div class="product-body">
                    <h3 class="product-name"><a href="#">${porCadaProducto.nombre}</a></h3>
                    <h4 class="product-price"><span class="qty">${porCadaProducto.cant}x</span>$${porCadaProducto.precio}</h4>
                </div>
                <button class="delete" onclick="deleteInCart('${porCadaProducto.id}')" ><i class="fa fa-close"></i></button>
            </div>
            `
                document.querySelector('#cartList').innerHTML += template;

                subtotal += porCadaProducto.precio * porCadaProducto.cant;
                cantItems += porCadaProducto.cant;
                document.querySelector("#cartTotals").innerHTML =
                    `
            <small>${cantItems} Item(s) selected</small>
            <h5>SUBTOTAL: $${subtotal}</h5>
            `
                document.querySelector("#cartQty").innerHTML = cantItems;
            }
        ))
}

showCart()

// BORRADO DE ARTICULO DEL CARRITO CON MÉTODO POST
// let deleteInCart = (idProducto) => {
//     console.log('Producto a borrar: ', idProducto)
//     let productoASubir = {};
//     productoASubir.id = idProducto;
//     console.log(productoASubir);

//     fetch('/cartdelete', {
//         method: 'POST',
//         headers: {
//             'Content-Type': 'application/json'
//             // 'Content-Type': 'application/x-www-form-urlencoded',
//           },
//           body: JSON.stringify(productoASubir)
//     })
//     .then(showCart())    
// }

//BORRADO DE ARTICULO DEL CARRITO CON MÉTODO DELETE
let deleteInCart = (idProducto) => {
    fetch('/cartdelete/' + idProducto, {
        method: 'DELETE'
    })
        .then(response => response.json())
        .then(showCart())
}
